# Russian translation for gnome-shell-extension-openweather
# Copyright (C) 2015
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# Gankov Andrey <gav@qsolution.ru>, 2011.
# Stas Solovey <whats_up@tut.by>, 2012.
# Timon <timosha@gmail.com>, 2013.
# Karbovnichiy Vasiliy <menstenebris@gmail.com>, 2015.
# Ivan Komaritsyn <vantu5z@mail.ru>, 2015, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.4\n"
"Report-Msgid-Bugs-To: https://gitlab.com/jenslody/gnome-shell-extension-"
"openweather/issues\n"
"POT-Creation-Date: 2019-03-12 23:11-0300\n"
"PO-Revision-Date: 2017-04-26 09:20+0300\n"
"Last-Translator: Ivan Komaritsyn <vantu5z@mail.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)\n"
"X-Generator: Gtranslator 2.91.7\n"

#: src/extension.js:185
msgid "..."
msgstr "..."

#: src/extension.js:448
msgid "Current location"
msgstr ""

#: src/extension.js:483
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Openweathermap.org не работает без API ключа.\n"
"В диалоге настроек либо включите использование ключа дополнения, либо "
"зарегистрируйтесь на https://openweathermap.org/appid и вставьте "
"персональный ключ в диалоге настроек."

#: src/extension.js:537
msgid ""
"Dark Sky does not work without an api-key.\n"
"Please register at https://darksky.net/dev/register and paste your personal "
"key into the preferences dialog."
msgstr ""
"Dark Sky не работает без API ключа.\n"
"Зарегистрируйтесь на https://darksky.net/dev/register и вставьте "
"персональный ключ в диалоге настроек."

#: src/extension.js:623
#, javascript-format
msgid "Can not connect to %s"
msgstr "Не удалось подключиться к %s"

#: src/extension.js:981 data/weather-settings.ui:454
msgid "Locations"
msgstr "Города"

#: src/extension.js:996
msgid "Reload Weather Information"
msgstr "Обновить информацию о погоде"

#: src/extension.js:1011
msgid "Weather data provided by:"
msgstr "Информация о погоде предоставлена:"

#: src/extension.js:1027
#, javascript-format
msgid "Can not open %s"
msgstr "Невозможно открыть %s"

#: src/extension.js:1034
msgid "Weather Settings"
msgstr "Настройки апплета погоды"

#: src/extension.js:1099 src/prefs.js:1075
msgid "Invalid city"
msgstr "Неправильный город"

#: src/extension.js:1110
msgid "Invalid location! Please try to recreate it."
msgstr "Неверное местоположение! Попробуйте создать его заново."

#: src/extension.js:1157 data/weather-settings.ui:780
msgid "°F"
msgstr "°F"

#: src/extension.js:1159 data/weather-settings.ui:781
msgid "K"
msgstr "K"

#: src/extension.js:1161 data/weather-settings.ui:782
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:1163 data/weather-settings.ui:783
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:1165 data/weather-settings.ui:784
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:1167 data/weather-settings.ui:785
msgid "°De"
msgstr "°De"

#: src/extension.js:1169 data/weather-settings.ui:786
msgid "°N"
msgstr "°N"

#: src/extension.js:1171 data/weather-settings.ui:779
msgid "°C"
msgstr "°C"

#: src/extension.js:1212
msgid "Calm"
msgstr "Штиль"

#: src/extension.js:1215
msgid "Light air"
msgstr "Тихий ветер"

#: src/extension.js:1218
msgid "Light breeze"
msgstr "Лёгкий ветер"

#: src/extension.js:1221
msgid "Gentle breeze"
msgstr "Слабый ветер"

#: src/extension.js:1224
msgid "Moderate breeze"
msgstr "Умеренный ветер"

#: src/extension.js:1227
msgid "Fresh breeze"
msgstr "Свежий ветер"

#: src/extension.js:1230
msgid "Strong breeze"
msgstr "Сильный ветер"

#: src/extension.js:1233
msgid "Moderate gale"
msgstr "Крепкий ветер"

#: src/extension.js:1236
msgid "Fresh gale"
msgstr "Очень крепкий ветер"

#: src/extension.js:1239
msgid "Strong gale"
msgstr "Шторм"

#: src/extension.js:1242
msgid "Storm"
msgstr "Сильный шторм"

#: src/extension.js:1245
msgid "Violent storm"
msgstr "Жестокий шторм"

#: src/extension.js:1248
msgid "Hurricane"
msgstr "Ураган"

#: src/extension.js:1252
msgid "Sunday"
msgstr "Воскресенье"

#: src/extension.js:1252
msgid "Monday"
msgstr "Понедельник"

#: src/extension.js:1252
msgid "Tuesday"
msgstr "Вторник"

#: src/extension.js:1252
msgid "Wednesday"
msgstr "Среда"

#: src/extension.js:1252
msgid "Thursday"
msgstr "Четверг"

#: src/extension.js:1252
msgid "Friday"
msgstr "Пятница"

#: src/extension.js:1252
msgid "Saturday"
msgstr "Суббота"

#: src/extension.js:1258
msgid "N"
msgstr "С"

#: src/extension.js:1258
msgid "NE"
msgstr "СВ"

#: src/extension.js:1258
msgid "E"
msgstr "В"

#: src/extension.js:1258
msgid "SE"
msgstr "ЮВ"

#: src/extension.js:1258
msgid "S"
msgstr "Ю"

#: src/extension.js:1258
msgid "SW"
msgstr "ЮЗ"

#: src/extension.js:1258
msgid "W"
msgstr "З"

#: src/extension.js:1258
msgid "NW"
msgstr "СЗ"

#: src/extension.js:1340 src/extension.js:1349 data/weather-settings.ui:817
msgid "hPa"
msgstr "гПа"

#: src/extension.js:1344 data/weather-settings.ui:818
msgid "inHg"
msgstr "дюйм рт.ст."

#: src/extension.js:1354 data/weather-settings.ui:819
msgid "bar"
msgstr "бар"

#: src/extension.js:1359 data/weather-settings.ui:820
msgid "Pa"
msgstr "Па"

#: src/extension.js:1364 data/weather-settings.ui:821
msgid "kPa"
msgstr "кПа"

#: src/extension.js:1369 data/weather-settings.ui:822
msgid "atm"
msgstr "атм"

#: src/extension.js:1374 data/weather-settings.ui:823
msgid "at"
msgstr "ат"

#: src/extension.js:1379 data/weather-settings.ui:824
msgid "Torr"
msgstr "торр"

#: src/extension.js:1384 data/weather-settings.ui:825
msgid "psi"
msgstr "psi"

#: src/extension.js:1389 data/weather-settings.ui:826
msgid "mmHg"
msgstr "мм рт.ст."

#: src/extension.js:1394 data/weather-settings.ui:827
msgid "mbar"
msgstr "мбар"

#: src/extension.js:1438 data/weather-settings.ui:801
msgid "m/s"
msgstr "м/с"

#: src/extension.js:1442 data/weather-settings.ui:800
msgid "mph"
msgstr "mph"

#: src/extension.js:1447 data/weather-settings.ui:799
msgid "km/h"
msgstr "км/ч"

#: src/extension.js:1456 data/weather-settings.ui:802
msgid "kn"
msgstr "уз"

#: src/extension.js:1461 data/weather-settings.ui:803
msgid "ft/s"
msgstr "фут/с"

#: src/extension.js:1555
msgid "Loading ..."
msgstr "Загрузка ..."

#: src/extension.js:1559
msgid "Please wait"
msgstr "Пожалуйста подождите"

#: src/extension.js:1620
msgid "Cloudiness:"
msgstr "Облачность:"

#: src/extension.js:1624
msgid "Humidity:"
msgstr "Влажность:"

#: src/extension.js:1628
msgid "Pressure:"
msgstr "Давление:"

#: src/extension.js:1632
msgid "Wind:"
msgstr "Ветер:"

#: src/darksky_net.js:159 src/darksky_net.js:291 src/openweathermap_org.js:352
#: src/openweathermap_org.js:475
msgid "Yesterday"
msgstr "Вчера"

#: src/darksky_net.js:162 src/darksky_net.js:294 src/openweathermap_org.js:354
#: src/openweathermap_org.js:477
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d день назад"
msgstr[1] "%d дня назад"
msgstr[2] "%d дней назад"

#: src/darksky_net.js:174 src/darksky_net.js:176 src/openweathermap_org.js:368
#: src/openweathermap_org.js:370
msgid ", "
msgstr ", "

#: src/darksky_net.js:271 src/openweathermap_org.js:469
msgid "Today"
msgstr "Сегодня"

#: src/darksky_net.js:287 src/openweathermap_org.js:471
msgid "Tomorrow"
msgstr "Завтра"

#: src/darksky_net.js:289 src/openweathermap_org.js:473
#, javascript-format
msgid "In %d day"
msgid_plural "In %d days"
msgstr[0] "На %d день"
msgstr[1] "На %d день"
msgstr[2] "На %d день"

#: src/openweathermap_org.js:183
msgid "Thunderstorm with light rain"
msgstr "Гроза с лёгким дождем"

#: src/openweathermap_org.js:185
msgid "Thunderstorm with rain"
msgstr "Гроза с дождем"

#: src/openweathermap_org.js:187
msgid "Thunderstorm with heavy rain"
msgstr "Гроза с сильным дождем"

#: src/openweathermap_org.js:189
msgid "Light thunderstorm"
msgstr "Лёгкая гроза"

#: src/openweathermap_org.js:191
msgid "Thunderstorm"
msgstr "Гроза"

#: src/openweathermap_org.js:193
msgid "Heavy thunderstorm"
msgstr "Сильная гроза"

#: src/openweathermap_org.js:195
msgid "Ragged thunderstorm"
msgstr "Возможна гроза"

#: src/openweathermap_org.js:197
msgid "Thunderstorm with light drizzle"
msgstr "Гроза с лёгкой моросью"

#: src/openweathermap_org.js:199
msgid "Thunderstorm with drizzle"
msgstr "Гроза с моросью"

#: src/openweathermap_org.js:201
msgid "Thunderstorm with heavy drizzle"
msgstr "Гроза с сильной моросью"

#: src/openweathermap_org.js:203
msgid "Light intensity drizzle"
msgstr "Слабая морось"

#: src/openweathermap_org.js:205
msgid "Drizzle"
msgstr "Морось"

#: src/openweathermap_org.js:207
msgid "Heavy intensity drizzle"
msgstr "Сильная морось"

#: src/openweathermap_org.js:209
msgid "Light intensity drizzle rain"
msgstr "Слабый моросящий дождь"

#: src/openweathermap_org.js:211
msgid "Drizzle rain"
msgstr "Моросящий дождь"

#: src/openweathermap_org.js:213
msgid "Heavy intensity drizzle rain"
msgstr "Сильный моросящий дождь"

#: src/openweathermap_org.js:215
msgid "Shower rain and drizzle"
msgstr "Ливень и морось"

#: src/openweathermap_org.js:217
msgid "Heavy shower rain and drizzle"
msgstr "Сильный ливень и морось"

#: src/openweathermap_org.js:219
msgid "Shower drizzle"
msgstr "Ливневый моросящий дождь"

#: src/openweathermap_org.js:221
msgid "Light rain"
msgstr "Лёгкий дождь"

#: src/openweathermap_org.js:223
msgid "Moderate rain"
msgstr "Умеренный дождь"

#: src/openweathermap_org.js:225
msgid "Heavy intensity rain"
msgstr "Сильный дождь"

#: src/openweathermap_org.js:227
msgid "Very heavy rain"
msgstr "Очень сильный дождь"

#: src/openweathermap_org.js:229
msgid "Extreme rain"
msgstr "Проливной дождь"

#: src/openweathermap_org.js:231
msgid "Freezing rain"
msgstr "Град"

#: src/openweathermap_org.js:233
msgid "Light intensity shower rain"
msgstr "Слабый ливень"

#: src/openweathermap_org.js:235
msgid "Shower rain"
msgstr "Ливень"

#: src/openweathermap_org.js:237
msgid "Heavy intensity shower rain"
msgstr "Сильный ливень"

#: src/openweathermap_org.js:239
msgid "Ragged shower rain"
msgstr "Локальный ливень"

#: src/openweathermap_org.js:241
msgid "Light snow"
msgstr "Лёгкий снег"

#: src/openweathermap_org.js:243
msgid "Snow"
msgstr "Снег"

#: src/openweathermap_org.js:245
msgid "Heavy snow"
msgstr "Сильный снег"

#: src/openweathermap_org.js:247
msgid "Sleet"
msgstr "Мокрый снег"

#: src/openweathermap_org.js:249
msgid "Shower sleet"
msgstr "Обильный мокрый снег"

#: src/openweathermap_org.js:251
msgid "Light rain and snow"
msgstr "Снег с лёгким дождём"

#: src/openweathermap_org.js:253
msgid "Rain and snow"
msgstr "Снег с дождём"

#: src/openweathermap_org.js:255
msgid "Light shower snow"
msgstr "Небольшой снегопад"

#: src/openweathermap_org.js:257
msgid "Shower snow"
msgstr "Снегопад"

#: src/openweathermap_org.js:259
msgid "Heavy shower snow"
msgstr "Сильный снегопад"

#: src/openweathermap_org.js:261
msgid "Mist"
msgstr "Мгла"

#: src/openweathermap_org.js:263
msgid "Smoke"
msgstr "Дымка"

#: src/openweathermap_org.js:265
msgid "Haze"
msgstr "Лёгкий туман"

#: src/openweathermap_org.js:267
msgid "Sand/Dust Whirls"
msgstr "Песчанные/Пылевые Вихри"

#: src/openweathermap_org.js:269
msgid "Fog"
msgstr "Густой туман"

#: src/openweathermap_org.js:271
msgid "Sand"
msgstr "Песок"

#: src/openweathermap_org.js:273
msgid "Dust"
msgstr "Пыль"

#: src/openweathermap_org.js:275
msgid "VOLCANIC ASH"
msgstr "ВУЛКАНИЧЕСКИЙ ПЕПЕЛ"

#: src/openweathermap_org.js:277
msgid "SQUALLS"
msgstr "ШКВАЛЬНЫЙ ВЕТЕР"

#: src/openweathermap_org.js:279
msgid "TORNADO"
msgstr "ТОРНАДО"

#: src/openweathermap_org.js:281
msgid "Sky is clear"
msgstr "Безоблачно"

#: src/openweathermap_org.js:283
msgid "Few clouds"
msgstr "Малооблачно"

#: src/openweathermap_org.js:285
msgid "Scattered clouds"
msgstr "Рассеянные облака"

#: src/openweathermap_org.js:287
msgid "Broken clouds"
msgstr "Облачность с просветами"

#: src/openweathermap_org.js:289
msgid "Overcast clouds"
msgstr "Сплошная облачность"

#: src/openweathermap_org.js:291
msgid "Not available"
msgstr "Недоступно"

#: src/prefs.js:202 src/prefs.js:256 src/prefs.js:303 src/prefs.js:310
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Неверные данные для поиска \"%s\""

#: src/prefs.js:210 src/prefs.js:272 src/prefs.js:316
#, javascript-format
msgid "\"%s\" not found"
msgstr "\"%s\" не найден"

#: src/prefs.js:376
msgid "Location"
msgstr "Город"

#: src/prefs.js:387
msgid "Provider"
msgstr "Источник"

#: src/prefs.js:564
#, javascript-format
msgid "Remove %s ?"
msgstr "Удалить %s ?"

#: src/prefs.js:1110
msgid "default"
msgstr "по умолчанию"

#: data/weather-settings.ui:31
msgid "Edit name"
msgstr "Редактировать имя"

#: data/weather-settings.ui:49 data/weather-settings.ui:79
#: data/weather-settings.ui:231
msgid "Clear entry"
msgstr "Очистить запись"

#: data/weather-settings.ui:63
msgid "Edit coordinates"
msgstr "Редактировать координаты"

#: data/weather-settings.ui:93 data/weather-settings.ui:268
msgid "Extensions default weather provider"
msgstr "Источник данных о погоде по умолчанию"

#: data/weather-settings.ui:125 data/weather-settings.ui:300
msgid "Cancel"
msgstr "Отменить"

#: data/weather-settings.ui:143 data/weather-settings.ui:318
msgid "Save"
msgstr "Сохранить"

#: data/weather-settings.ui:208
msgid "Search by location or coordinates"
msgstr "Поиск по названию или координатам"

#: data/weather-settings.ui:232
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "Например: Москва, Тверь или -8.5211767,179.1976747"

#: data/weather-settings.ui:242
msgid "Find"
msgstr "Найти"

#: data/weather-settings.ui:475
msgid "Chose default weather provider"
msgstr "Выбор источника данных о погоде по умолчанию"

#: data/weather-settings.ui:488
msgid "Personal Api key from openweathermap.org"
msgstr "Персональный API ключ от openweathermap.org"

#: data/weather-settings.ui:539
msgid "Personal Api key from Dark Sky"
msgstr "Персональный API ключ от Dark Sky"

#: data/weather-settings.ui:552
msgid "Refresh timeout for current weather [min]"
msgstr "Периодичность обновления текущей погоды [мин.]"

#: data/weather-settings.ui:566
msgid "Refresh timeout for weather forecast [min]"
msgstr "Периодичность обновления прогноза погоды [мин.]"

#: data/weather-settings.ui:594
msgid ""
"Note: the forecast-timout is not used for Dark Sky, because they do not "
"provide seperate downloads for current weather and forecasts."
msgstr ""
"Примечание: периодичность обновления прогноза погоды не используется для "
"Dark Sky, потому что он не поддерживает раздельную загрузку для текущей "
"погоды и прогноза."

#: data/weather-settings.ui:622
msgid "Use extensions api-key for openweathermap.org"
msgstr "Использовать API ключ дополнения от openweathermap.org"

#: data/weather-settings.ui:633
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""
"Отключите, если у Вас есть собственный API ключ от openweathermap.org и "
"введите его в поле ниже."

#: data/weather-settings.ui:650
msgid "Weather provider"
msgstr "Источник данных о погоде"

#: data/weather-settings.ui:670
msgid "Chose geolocation provider"
msgstr "Выбор источника геоданных"

#: data/weather-settings.ui:696
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Персональный AppKey от developer.mapquest.com"

#: data/weather-settings.ui:725
msgid "Geolocation provider"
msgstr "Источник геоданных"

#: data/weather-settings.ui:745
#: data/org.gnome.shell.extensions.openweather.gschema.xml:63
msgid "Temperature Unit"
msgstr "Единицы измерения температуры"

#: data/weather-settings.ui:756
msgid "Wind Speed Unit"
msgstr "Единицы измерения скорости ветра"

#: data/weather-settings.ui:767
#: data/org.gnome.shell.extensions.openweather.gschema.xml:67
msgid "Pressure Unit"
msgstr "Единицы измерения давления"

#: data/weather-settings.ui:804
msgid "Beaufort"
msgstr "по шкале Бофорта"

#: data/weather-settings.ui:844
msgid "Units"
msgstr "Единицы измерения"

#: data/weather-settings.ui:864
#: data/org.gnome.shell.extensions.openweather.gschema.xml:113
msgid "Position in Panel"
msgstr "Положение на панели"

#: data/weather-settings.ui:875
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "Положение окна в [%] от 0 (слева) до 100 (справа)"

#: data/weather-settings.ui:886
#: data/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Wind Direction by Arrows"
msgstr "Отображать направление ветра стрелками"

#: data/weather-settings.ui:897
#: data/org.gnome.shell.extensions.openweather.gschema.xml:89
msgid "Translate Conditions"
msgstr "Переводить погодные условия"

#: data/weather-settings.ui:908
#: data/org.gnome.shell.extensions.openweather.gschema.xml:93
msgid "Symbolic Icons"
msgstr "Использовать символические значки"

#: data/weather-settings.ui:919
msgid "Text on buttons"
msgstr "Текст на кнопках"

#: data/weather-settings.ui:930
#: data/org.gnome.shell.extensions.openweather.gschema.xml:101
msgid "Temperature in Panel"
msgstr "Отображать температуру на панели"

#: data/weather-settings.ui:941
#: data/org.gnome.shell.extensions.openweather.gschema.xml:105
msgid "Conditions in Panel"
msgstr "Отображать погодные условия на панели"

#: data/weather-settings.ui:952
#: data/org.gnome.shell.extensions.openweather.gschema.xml:109
msgid "Conditions in Forecast"
msgstr "Отображать погодные условия в прогнозе"

#: data/weather-settings.ui:963
msgid "Center forecast"
msgstr "Центрировать прогноз"

#: data/weather-settings.ui:974
#: data/org.gnome.shell.extensions.openweather.gschema.xml:137
msgid "Number of days in forecast"
msgstr "Число дней в прогнозе"

#: data/weather-settings.ui:985
#: data/org.gnome.shell.extensions.openweather.gschema.xml:141
msgid "Maximal number of digits after the decimal point"
msgstr "Число знаков после запятой"

#: data/weather-settings.ui:997
msgid "Center"
msgstr "По центру"

#: data/weather-settings.ui:998
msgid "Right"
msgstr "Справа"

#: data/weather-settings.ui:999
msgid "Left"
msgstr "Слева"

#: data/weather-settings.ui:1154
msgid "Maximal number of characters in location label"
msgstr ""

#: data/weather-settings.ui:1185
msgid "Layout"
msgstr "Отображение"

#: data/weather-settings.ui:1237
msgid "Version: "
msgstr "Версия: "

#: data/weather-settings.ui:1251
msgid "unknown (self-build ?)"
msgstr "неизвестно (собственная сборка ?)"

#: data/weather-settings.ui:1271
msgid ""
"<span>Weather extension to display weather information from <a href="
"\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
"darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
msgstr ""
"<span>Дополнение для отображения сведений о погоде от <a href=\"https://"
"openweathermap.org/\">Openweathermap</a> или <a href=\"https://darksky.net"
"\">Dark Sky</a> для почти всех мест в мире.</span>"

#: data/weather-settings.ui:1294
msgid "Maintained by"
msgstr "Автор"

#: data/weather-settings.ui:1324
msgid "Webpage"
msgstr "Домашняя страница"

#: data/weather-settings.ui:1344
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Эта программа распространяется БЕЗ КАКИХ ЛИБО "
"ГАРАНТИЙ.\n"
"Смотри <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, версии 2 или позднее</a> для информации.</"
"span>"

#: data/weather-settings.ui:1365
msgid "About"
msgstr "О себе"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:55
msgid "Weather Provider"
msgstr "Источник данных о погоде"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:59
msgid "Geolocation Provider"
msgstr "Источник геоданных"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid "Wind Speed Units"
msgstr "Единицы измерения скорости ветра"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:72
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""
"Выберите единицу изменения скорости ветра. Допустимые значения: км/ч, мили в "
"час, м/с, узлы, фут/с или по шкале Бофорта."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:77
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "Выберите как отображать направления ветра - стрелками или буквами."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:81
msgid "City to be displayed"
msgstr "Город для отображения"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:85
msgid "Actual City"
msgstr "Текущий город"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:97
msgid "Use text on buttons in menu"
msgstr "Отображать текст на кнопках в меню"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:117
msgid "Horizontal position of menu-box."
msgstr "Положение окна по горизонтали."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:121
msgid "Refresh interval (actual weather)"
msgstr "Интервал обновления (текущая погода)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:125
msgid "Maximal length of the location text"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:129
msgid "Refresh interval (forecast)"
msgstr "Интервал обновления (прогноз)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:133
msgid "Center forecastbox."
msgstr "Центрировать прогноз."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:145
msgid "Your personal API key from openweathermap.org"
msgstr "Персональный API ключ от openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:149
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Использовать API ключ дополнения от openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:153
msgid "Your personal API key from Dark Sky"
msgstr "Персональный API ключ от Dark Sky"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:157
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Персональный AppKey от developer.mapquest.com"
